#include "timer.h"

static event_queue_t *__timer_event_queue;
volatile static bool __timer_tick = false;

static inline int8_t __timer_get_system_prescaler(void);

int8_t timer_init()
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1C = 0;
    TIMSK1 = 0;
    TIFR1 = 0b00101111;

    int8_t prescaler = __timer_get_system_prescaler();
    if (prescaler < 0)
        return -1;

    // Set up event queue.
    __timer_event_queue = event_queue_init();

    // Set up timer for 1 kHz with interrupt in CTC mode
    OCR1A = F_CPU / (prescaler * 1000);
    TIMSK1 = 1 << OCIE1A;
    TCCR1B = (1 << WGM12) | (1 << CS10);
    return 0;
}

void timer_register_callback(void (*callback)(void))
{
    event_queue_subscribe(__timer_event_queue, callback);
}

void timer_deregister_callback(void (*callback)(void))
{
    event_queue_unsubscribe(__timer_event_queue, callback);
}

void timer_task()
{
    if (__timer_tick)
    {
        __timer_tick = false;
        event_queue_signal(__timer_event_queue);
    }
}

ISR(TIMER1_COMPA_vect)
{
    __timer_tick = true;
}

static inline int8_t __timer_get_system_prescaler()
{
    const uint8_t prescaler_bits = CLKPR & 0b1111;
    if (prescaler_bits > 8)
        return -1;
    return pow(2, prescaler_bits);
}
