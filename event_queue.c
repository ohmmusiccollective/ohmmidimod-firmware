#include "event_queue.h"

typedef struct __event_queue_list_t __event_queue_list_t;
struct __event_queue_list_t {
  event_queue_t* event_queue;
  __event_queue_list_t* next;
};
static __event_queue_list_t* __event_queue_list = NULL;

event_queue_t* event_queue_init()
{
  event_queue_t* event_queue = malloc(sizeof(event_queue_t));
  event_queue->callbacks = NULL;
  event_queue->flag = false;
  event_queue->next = NULL;

  // Create a list entry for the new event queue.
  __event_queue_list_t* event_queue_list_entry = (__event_queue_list_t*) malloc(sizeof(__event_queue_list_t));
  event_queue_list_entry->event_queue = event_queue;
  event_queue_list_entry->next = NULL;

  // Append the entry to the list.
  __event_queue_list_t* event_queue_list_ptr = __event_queue_list;
  if (!event_queue_list_ptr) {
    // First event queue
    __event_queue_list = event_queue_list_entry;
  } else {
    // Skip to the end of the list.
    while (event_queue_list_ptr && event_queue_list_ptr->next) {
      event_queue_list_ptr = event_queue_list_ptr->next;
    }
    event_queue_list_ptr->next = event_queue_list_entry;
  }
  
  return event_queue;
}

void event_queue_subscribe(event_queue_t* event_queue, void (*callback)(void))
{
  // Create a new callback list entry.
  event_queue_callback_t* callback_entry_ptr = (event_queue_callback_t*) malloc(sizeof(event_queue_callback_t));
  callback_entry_ptr->callback = callback;
  callback_entry_ptr->next = NULL;

  // Append the entry to the list.
  event_queue_callback_t* callback_list_ptr = event_queue->callbacks;
  if (!callback_list_ptr) {
    // First entry
    event_queue->callbacks = callback_entry_ptr;
  } else {
    // Skip to the end of the list.
    while (callback_list_ptr && callback_list_ptr->next) {
      callback_list_ptr = callback_list_ptr->next;
    }
    callback_list_ptr->next = callback_entry_ptr;
  }
}

void event_queue_signal(event_queue_t* event_queue)
{
  event_queue->flag = true;
}

void event_queue_unsubscribe(event_queue_t* event_queue, void (*callback)(void))
{
  event_queue_callback_t* callback_prev_ptr = NULL;
  event_queue_callback_t* callback_curr_ptr = event_queue->callbacks;
  while (callback_curr_ptr) {

    if (callback_curr_ptr->callback == callback) {
      if (callback_prev_ptr) {
        callback_prev_ptr->next = callback_curr_ptr->next;
      } else {
        // Deleting the first entry.
        event_queue->callbacks = callback_curr_ptr->next;
      }
      free(callback_curr_ptr);
      return;
    }

    callback_prev_ptr = callback_curr_ptr;
    callback_curr_ptr = callback_curr_ptr->next;
  }
  
}

void event_queue_destroy(event_queue_t* event_queue)
{
  __event_queue_list_t* event_queue_list_prev_ptr = NULL;
  __event_queue_list_t* event_queue_list_curr_ptr = __event_queue_list;

  while (event_queue_list_curr_ptr) {

    if (event_queue_list_curr_ptr->event_queue == event_queue) {
      
      // Free all callback entries.
      event_queue_callback_t* callback_ptr = event_queue->callbacks;
      while (callback_ptr) {
        event_queue_callback_t* callback_next_ptr = callback_ptr->next;
        free(callback_ptr);
        callback_ptr = callback_next_ptr;
      }

      if (event_queue_list_prev_ptr) {
        event_queue_list_prev_ptr->next = event_queue_list_curr_ptr->next;
      } else {
        __event_queue_list = event_queue_list_curr_ptr->next;
      }
      free(event_queue_list_curr_ptr);
      return;
    }

    event_queue_list_prev_ptr = event_queue_list_curr_ptr;
    event_queue_list_curr_ptr = event_queue_list_curr_ptr->next;
  }
}

void event_queue_task()
{
  __event_queue_list_t* event_queue_list_ptr = __event_queue_list;
  while (event_queue_list_ptr) {
    if (event_queue_list_ptr->event_queue->flag) {

      event_queue_callback_t* callback_list_ptr = event_queue_list_ptr->event_queue->callbacks;
      while (callback_list_ptr) {
        if (callback_list_ptr->callback) {
          callback_list_ptr->callback();
        }
        callback_list_ptr = callback_list_ptr->next;
      }
      
      event_queue_list_ptr->event_queue->flag = false;
    }
    event_queue_list_ptr = event_queue_list_ptr->next;
  }
}
