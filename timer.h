/**
 * Driver for Timer/Counter1.
 * 
 * Resources:
 *  - Timer/Counter1
 * 
 * Pins:
 * 
 */

#ifndef _TIMER_H_
#define _TIMER_H_

#include <math.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#include "event_queue.h"

/**
 * Initialize the timer.
 * Returns 0 on success or a negative number on failure.
 */
int8_t timer_init(void);

/**
 * Register a callback to be invoked on each tick.
 * Attempting to register an already registered callback will have no effect.
 */
void timer_register_callback(void (*callback)(void));

/**
 * De-register a callback.
 * Attempting to de-register a callback that is not registered will have no effect.
 */
void timer_deregister_callback(void (*callback)(void));

/**
 * Call this repeatedly in the main loop.
 */
void timer_task(void);

#endif
