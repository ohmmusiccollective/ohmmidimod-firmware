#ifdef __cplusplus
extern "C" {
#endif

#ifndef _EVENT_QUEUE_H
#define _EVENT_QUEUE_H

#include <stdbool.h>
#include <stdlib.h>

typedef struct event_queue_callback_t event_queue_callback_t;
struct event_queue_callback_t {
   void (*callback)(void);
   event_queue_callback_t* next;
};

typedef struct event_queue_t event_queue_t;
struct event_queue_t {
  event_queue_callback_t* callbacks;
  volatile bool flag;
  event_queue_t* next;
};

event_queue_t* event_queue_init(void);
void event_queue_subscribe(event_queue_t* event_queue, void (*callback)(void));
void event_queue_signal(event_queue_t* event_queue);
void event_queue_unsubscribe(event_queue_t* event_queue, void (*callback)(void));
void event_queue_destroy(event_queue_t* event_queue);
void event_queue_task(void);

#endif

#ifdef __cplusplus
}
#endif
