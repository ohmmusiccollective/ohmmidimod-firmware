#include "led.h"

#define LED_PRESCALER 50    // effective frequency: 1 kHz / 50 = 20 Hz
#define LED_PULSE_PERIOD 20 // 20 * 50 ms = 1 s ~ 1 Hz
#define LED_BLINK_PERIOD 10 // 10 * 50 ms = 500 ms ~ 1 Hz

static led_state_e __led_state = LED_STATE_OFF;
static uint16_t __led_pulse_count = 0;
static uint16_t __led_blink_count = 0;
static uint16_t __led_counter = 0;

static void __led_timer_callback(void);
static inline void __led_pwm_start(void);
static inline void __led_pwm_stop(void);
static inline void __led_pwm_set(uint8_t);
static inline void __led_on(void);
static inline void __led_off(void);
static inline void __led_toggle(void);
static inline uint8_t __led_pulse_get_amplitude(void);

void led_init()
{
    // Set PB7 as output.
    DDRB |= 1 << DDB7;
    PORTB &= ~(1 << PORTB7);
}

void led_set_state(led_state_e state)
{
    if (state == __led_state)
        return;
    
    __led_state = state;
    switch (__led_state)
    {
    case LED_STATE_OFF:
        timer_deregister_callback(__led_timer_callback);
        __led_pwm_stop();
        __led_off();
        break;

    case LED_STATE_ON:
        timer_deregister_callback(__led_timer_callback);
        __led_pwm_stop();
        __led_on();
        break;

    case LED_STATE_BLINK:
        timer_register_callback(__led_timer_callback);
        __led_pwm_stop();
        __led_blink_count = 0;
        break;

    case LED_STATE_DIM:
        timer_deregister_callback(__led_timer_callback);
        __led_pwm_start();
        break;

    case LED_STATE_PULSE_HALF_CIRCLE:
    case LED_STATE_PULSE_SINE:
    case LED_STATE_PULSE_TRIANGLE:
        timer_register_callback(__led_timer_callback);
        __led_pulse_count = 0;
        __led_pwm_start();
        break;

    default:
        break;
    }
}

void led_set_brightness(uint8_t brightness)
{
    if (__led_state == LED_STATE_DIM)
    {
        __led_pwm_set(brightness);
    }
}

static void __led_timer_callback()
{
    if (++__led_counter < LED_PRESCALER)
        return;
    __led_counter = 0;

    switch (__led_state)
    {
    case LED_STATE_PULSE_HALF_CIRCLE:
    case LED_STATE_PULSE_SINE:
    case LED_STATE_PULSE_TRIANGLE:
        __led_pwm_set(__led_pulse_get_amplitude());
        __led_pulse_count = (__led_pulse_count + 1) % LED_PULSE_PERIOD;
        break;

    case LED_STATE_BLINK:
        if (++__led_blink_count == LED_BLINK_PERIOD)
        {
            __led_blink_count = 0;
            __led_toggle();
        }
        break;

    default:
        break;
    }
}

static inline void __led_pwm_start()
{
    // Attach Timer/Counter0 PWM output to PB7
    // Fast PWM mode, /64 prescaler, duty cycle determined by value in OCR0A
    // PWM frequency: 978 Hz
    TCCR0A = (1 << COM0A1) | (0 << COM0A0) | (1 << WGM01) | (1 << WGM00);
    TCCR0B = (0 << WGM02) | (0 << CS02) | (1 << CS01) | (1 << CS00);
    __led_pwm_set(0);
}

static inline void __led_pwm_stop()
{
    TCCR0A = 0;
    TCCR0B = 0;
}

static inline void __led_pwm_set(uint8_t top)
{
    OCR0A = top;
}

static inline void __led_on()
{
    PORTB |= (1 << PORTB7);
}

static inline void __led_off()
{
    PORTB &= ~(1 << PORTB7);
}

static inline void __led_toggle()
{
    PINB |= (1 << PINB7);
}

static inline uint8_t __led_pulse_get_amplitude()
{
    switch (__led_state)
    {
    case LED_STATE_PULSE_HALF_CIRCLE:
        return (uint8_t)(255. * sqrt(1. - square(2. * __led_pulse_count / LED_PULSE_PERIOD - 1.)));
    case LED_STATE_PULSE_SINE:
        return (uint8_t)(127. + 127. * sin(2. * M_PI * __led_pulse_count / LED_PULSE_PERIOD));
    case LED_STATE_PULSE_TRIANGLE:
        return (uint8_t)(255. * (1. - fabs(2. * __led_pulse_count / LED_PULSE_PERIOD - 1.)));
    default:
        return 0;
    }
}
