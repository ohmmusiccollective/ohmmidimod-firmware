#include "ohmmidimod.h"

static USB_ClassInfo_MIDI_Device_t Keyboard_MIDI_Interface =
	{
		.Config =
			{
				.StreamingInterfaceNumber = INTERFACE_ID_AudioStream,
				.DataOUTEndpoint =
					{
						.Address = MIDI_STREAM_OUT_EPADDR,
						.Size = MIDI_STREAM_EPSIZE,
						.Banks = 1,
					},
			},
};

#define JUNO_CHANNEL_EMPTY 0x61
#define JUNO_KEY_CODE 0xFE
#define JUNO_END_MARK 0xFF
#define JUNO_NUM_CHANNELS 6
static uint8_t channel_map[JUNO_NUM_CHANNELS] = {
	JUNO_CHANNEL_EMPTY,
	JUNO_CHANNEL_EMPTY,
	JUNO_CHANNEL_EMPTY,
	JUNO_CHANNEL_EMPTY,
	JUNO_CHANNEL_EMPTY,
	JUNO_CHANNEL_EMPTY,
};
volatile static bool fresh = false;

void dcb_callback(void)
{
	switch (dcb_get_connection_state())
	{
	case DCB_CONNECTION_STATE_CONNECTED:
		led_set_state(LED_STATE_ON);
		break;
	case DCB_CONNECTION_STATE_DISCONNECTED:
		led_set_state(LED_STATE_PULSE_SINE);
		break;
	}
}

int main(void)
{
	if (SetupHardware() < 0)
		return -1;

	sei();

	MIDI_EventPacket_t ReceivedMIDIEvent;
	unsigned char gate;
	unsigned char pitch;
	unsigned char dcb_key_event;

	for (;;)
	{
		// Receive MIDI events
		while (MIDI_Device_ReceiveEventPacket(&Keyboard_MIDI_Interface, &ReceivedMIDIEvent))
		{
			// Anything that is not NOTE_ON or NOTE_OFF is ignored.
			if (!(ReceivedMIDIEvent.Event == MIDI_EVENT(0, MIDI_COMMAND_NOTE_ON) || ReceivedMIDIEvent.Event == MIDI_EVENT(0, MIDI_COMMAND_NOTE_OFF)))
			{
				continue;
			}

			// JUNO-60 accepts notes in the range of C0 (MIDI 12) and C8 (MIDI 108).
			// Anything outside that range is ignored.
			if (ReceivedMIDIEvent.Data2 < 12 || ReceivedMIDIEvent.Data2 > 108)
			{
				continue;
			}

			// At this point, we have new data to send
			fresh = true;

			// DCB pitch = MIDI pitch - 12
			gate = ReceivedMIDIEvent.Event == MIDI_EVENT(0, MIDI_COMMAND_NOTE_ON);
			pitch = ReceivedMIDIEvent.Data2 - 12;
			dcb_key_event = (gate << 7) | pitch;

			if (gate)
			{
				// If it's a NOTE_ON event, find a free channel to map it to.
				// If there is no free channel, the event is ignored.
				for (unsigned int i = 0; i < JUNO_NUM_CHANNELS; ++i)
				{
					if (channel_map[i] == JUNO_CHANNEL_EMPTY)
					{
						channel_map[i] = dcb_key_event;
						break;
					}
				}
			}
			else
			{
				// If it's a NOTE_OFF event, find the channel it is on and turn the gate off.
				for (unsigned int i = 0; i < JUNO_NUM_CHANNELS; ++i)
				{
					if (channel_map[i] == ((1 << 7) | pitch))
					{
						channel_map[i] = dcb_key_event;
					}
				}
			}
		}

		// Only write to the JUNO if there is new data.
		if (fresh)
		{

			fresh = false;

			// Key code start byte
			dcb_write(JUNO_KEY_CODE);

			// Channel bytes
			for (unsigned int i = 0; i < JUNO_NUM_CHANNELS; ++i)
			{
				dcb_write(channel_map[i]);
			}

			// End mark
			dcb_write(JUNO_END_MARK);

			// Clear the NOTE_OFF events from the channel map.
			for (unsigned int i = 0; i < JUNO_NUM_CHANNELS; ++i)
			{
				if (!(channel_map[i] & (1 << 7)))
				{
					channel_map[i] = JUNO_CHANNEL_EMPTY;
				}
			}
		}

		// Call the driver tasks.
		MIDI_Device_USBTask(&Keyboard_MIDI_Interface);
		USB_USBTask();
		timer_task();
		dcb_task();
		event_queue_task();
	} // for (;;)
}

int8_t SetupHardware(void)
{
	// Initialize drivers.
	event_queue_init();
	USB_Init();
	if (timer_init() < 0)
		return -1;
	dcb_init();
	led_init();

	// Start out in "DISCONNECTED" state.
	led_set_state(LED_STATE_PULSE_SINE);
	dcb_register_callback(dcb_callback);

	return 0;
}

void EVENT_USB_Device_Connect(void)
{
}

void EVENT_USB_Device_Disconnect(void)
{
}

void EVENT_USB_Device_ConfigurationChanged(void)
{
	bool ConfigSuccess = true;

	ConfigSuccess &= MIDI_Device_ConfigureEndpoints(&Keyboard_MIDI_Interface);
}

void EVENT_USB_Device_ControlRequest(void)
{
	MIDI_Device_ProcessControlRequest(&Keyboard_MIDI_Interface);
}
