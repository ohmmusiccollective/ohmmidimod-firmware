#include "dcb.h"

#define __DCB_CONNECTION_STATE_TIMEOUT_THRESHOLD 1024
volatile static dcb_connection_state_e __dcb_connection_state = DCB_CONNECTION_STATE_DISCONNECTED;
volatile static uint32_t __dcb_connection_state_timeout = 0;
volatile static bool __dcb_connection_state_changed = false;
static event_queue_t *__dcb_event_queue;

static void __dcb_timer_callback(void);

void dcb_init()
{
    // Set baud rate: 31.25 kHz
    UBRR1 = 255;

    // Enable transmitter
    UCSR1B = (1 << TXEN1);

    // Set frame format: 8 data bits, 2 stop bits, synchronous USART, odd parity, data changes on rising clock edge.
    // UCPOL1 must be 0 if there is no inverter between the pin and the DCB connector, otherwise 1
    UCSR1C = (1 << USBS1) | (3 << UCSZ10) | (0 << UMSEL11) | (1 << UMSEL10) | (1 << UPM11) | (1 << UPM10) | (1 << UCPOL1);

    // For synchronous mode when the MCU generates the clock, XCK / PD5 must be set to output
    DDRD |= (1 << DDD5);

    // Register timer callback
    timer_register_callback(__dcb_timer_callback);

    // Set up event queue.
    __dcb_event_queue = event_queue_init();

    // Set PD4 as input
    DDRD &= ~(1 << DDD4);

    // Trigger INT5 on falling edge
    EICRB |= (1 << ISC51);

    // Enable INT5
    EIMSK |= (1 << INT5);
}

void dcb_write(uint8_t data)
{
    // Wait for empty transmit buffer
    while (!(UCSR1A & (1 << UDRE1)))
        ;
    UDR1 = data;
}

dcb_connection_state_e dcb_get_connection_state(void)
{
    return __dcb_connection_state;
}

void dcb_register_callback(void (*callback)(void))
{
    event_queue_subscribe(__dcb_event_queue, callback);
}

void dcb_deregister_callback(void (*callback)(void))
{
    event_queue_unsubscribe(__dcb_event_queue, callback);
}

void dcb_task()
{
    if (__dcb_connection_state_changed)
    {
        __dcb_connection_state_changed = false;

        event_queue_signal(__dcb_event_queue);
    }
}

void __dcb_timer_callback()
{
    if (__dcb_connection_state == DCB_CONNECTION_STATE_DISCONNECTED)
        return;

    if (++__dcb_connection_state_timeout >= __DCB_CONNECTION_STATE_TIMEOUT_THRESHOLD)
    {
        __dcb_connection_state = DCB_CONNECTION_STATE_DISCONNECTED;
        __dcb_connection_state_changed = true;
    }
}

ISR(INT5_vect)
{
    __dcb_connection_state_timeout = 0;
    if (__dcb_connection_state == DCB_CONNECTION_STATE_DISCONNECTED)
    {
        __dcb_connection_state = DCB_CONNECTION_STATE_CONNECTED;
        __dcb_connection_state_changed = true;
    }
}
