# OHMMIDIMOD Firmware

This is the firmware for the [OHMMIDIMOD](https://gitlab.com/ohmmusiccollective/ohmmidimod-hardware) MIDI-to-DCB converter.
It uses the MIDI device class driver of the [LUFA](http://www.fourwalledcubicle.com/LUFA.php) USB library.

## Building the Firmware

We use [avr-gcc](https://www.microchip.com/en-us/development-tools-tools-and-software/gcc-compilers-avr-and-arm) to build the code.
[avrdude](https://www.nongnu.org/avrdude/) is the most lightweight way of uploading the firmware to the device,
although [MPLAB X IDE](https://www.microchip.com/en-us/development-tools-tools-and-software/mplab-x-ide) is much more convenient
when making changes to the code as it also supports debugging using the chip's debugWire interface.

### When first setting up repository

    git submodule update --init --recursive

### Compile

    make all

### Upload using Atmel ICE

    # Initial setting of clock source fuse byte.
    # External 16 MHz oscillator is used with maximum startup time.
    # Clock pre-scaler is disabled.
    avrdude -p atmega16u2 -c atmelice_isp -U lfuse:w:0xFF:m

    # Uploading of actual firmware
    avrdude -p atmega16u2 -c atmelice_isp -U flash:w:ohmmidimod.hex

## Acknowledgements

We would like to thank [pid.codes](https://pid.codes) for allocating the `0x1209 / 0xB0DE` USB VID / PID combination to us.
