/**
 * Handles anything related to the DCB interface.
 * 
 * Resources:
 *  - USART1
 *  - INT5
 * 
 * Pins:
 *  - PD3 / TXD1
 *  - PD4 / INT5
 *  - PD5 / XCK1
 */

#ifndef _DCB_H_
#define _DCB_H_

#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

#include "event_queue.h"
#include "timer.h"

#define DCB_NOTE_ON(note) (note | (1 << 7))
#define DCB_NOTE_OFF(note) (note & ~(1 << 7))

typedef enum
{
    DCB_CONNECTION_STATE_DISCONNECTED = 0,
    DCB_CONNECTION_STATE_CONNECTED,
} dcb_connection_state_e;

void dcb_init(void);
void dcb_write(uint8_t data);
dcb_connection_state_e dcb_get_connection_state(void);
void dcb_register_callback(void (*callback)(void));
void dcb_deregister_callback(void (*callback)(void));
void dcb_task(void);

#endif
