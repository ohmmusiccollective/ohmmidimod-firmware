#include "descriptors.h"

const USB_Descriptor_Device_t PROGMEM DeviceDescriptor =
	{
		.Header = {.Size = sizeof(USB_Descriptor_Device_t), .Type = DTYPE_Device},

		.USBSpecification = VERSION_BCD(1, 1, 0),
		.Class = USB_CSCP_NoDeviceClass,
		.SubClass = USB_CSCP_NoDeviceSubclass,
		.Protocol = USB_CSCP_NoDeviceProtocol,

		.Endpoint0Size = FIXED_CONTROL_ENDPOINT_SIZE,

		.VendorID = 0x1209,
		.ProductID = 0xB0DE,
		.ReleaseNumber = VERSION_BCD(0, 0, 1),

		.ManufacturerStrIndex = STRING_ID_Manufacturer,
		.ProductStrIndex = STRING_ID_Product,
		.SerialNumStrIndex = USE_INTERNAL_SERIAL,

		.NumberOfConfigurations = FIXED_NUM_CONFIGURATIONS};

const USB_Descriptor_Configuration_t PROGMEM ConfigurationDescriptor =
	{
		.Config =
			{
				.Header = {.Size = sizeof(USB_Descriptor_Configuration_Header_t), .Type = DTYPE_Configuration},

				.TotalConfigurationSize = sizeof(USB_Descriptor_Configuration_t),
				.TotalInterfaces = 2,

				.ConfigurationNumber = 1,
				.ConfigurationStrIndex = NO_DESCRIPTOR,

				.ConfigAttributes = (USB_CONFIG_ATTR_RESERVED | USB_CONFIG_ATTR_SELFPOWERED),

				.MaxPowerConsumption = USB_CONFIG_POWER_MA(100)},

		.Audio_ControlInterface =
			{
				.Header = {.Size = sizeof(USB_Descriptor_Interface_t), .Type = DTYPE_Interface},

				.InterfaceNumber = INTERFACE_ID_AudioControl,
				.AlternateSetting = 0,

				.TotalEndpoints = 0,

				.Class = AUDIO_CSCP_AudioClass,
				.SubClass = AUDIO_CSCP_ControlSubclass,
				.Protocol = AUDIO_CSCP_ControlProtocol,

				.InterfaceStrIndex = NO_DESCRIPTOR},

		.Audio_ControlInterface_SPC =
			{
				.Header = {.Size = sizeof(USB_Audio_Descriptor_Interface_AC_t), .Type = AUDIO_DTYPE_CSInterface},
				.Subtype = AUDIO_DSUBTYPE_CSInterface_Header,

				.ACSpecification = VERSION_BCD(1, 0, 0),
				.TotalLength = sizeof(USB_Audio_Descriptor_Interface_AC_t),

				.InCollection = 1,
				.InterfaceNumber = INTERFACE_ID_AudioStream,
			},

		.Audio_StreamInterface =
			{
				.Header = {.Size = sizeof(USB_Descriptor_Interface_t), .Type = DTYPE_Interface},

				.InterfaceNumber = INTERFACE_ID_AudioStream,
				.AlternateSetting = 0,

				.TotalEndpoints = 1,

				.Class = AUDIO_CSCP_AudioClass,
				.SubClass = AUDIO_CSCP_MIDIStreamingSubclass,
				.Protocol = AUDIO_CSCP_StreamingProtocol,

				.InterfaceStrIndex = NO_DESCRIPTOR},

		.Audio_StreamInterface_SPC =
			{
				.Header = {.Size = sizeof(USB_MIDI_Descriptor_AudioInterface_AS_t), .Type = AUDIO_DTYPE_CSInterface},
				.Subtype = AUDIO_DSUBTYPE_CSInterface_General,

				.AudioSpecification = VERSION_BCD(1, 0, 0),

				.TotalLength = (sizeof(USB_Descriptor_Configuration_t) -
								offsetof(USB_Descriptor_Configuration_t, Audio_StreamInterface_SPC))},

		.MIDI_In_Jack_Emb =
			{
				.Header = {.Size = sizeof(USB_MIDI_Descriptor_InputJack_t), .Type = AUDIO_DTYPE_CSInterface},
				.Subtype = AUDIO_DSUBTYPE_CSInterface_InputTerminal,

				.JackType = MIDI_JACKTYPE_Embedded,
				.JackID = 0x01,

				.JackStrIndex = NO_DESCRIPTOR},

		.MIDI_In_Jack_Endpoint =
			{
				.Endpoint =
					{
						.Header = {.Size = sizeof(USB_Audio_Descriptor_StreamEndpoint_Std_t), .Type = DTYPE_Endpoint},

						.EndpointAddress = MIDI_STREAM_OUT_EPADDR,
						.Attributes = (EP_TYPE_BULK | ENDPOINT_ATTR_NO_SYNC | ENDPOINT_USAGE_DATA),
						.EndpointSize = MIDI_STREAM_EPSIZE,
						.PollingIntervalMS = 0x05},

				.Refresh = 0,
				.SyncEndpointNumber = 0},

		.MIDI_In_Jack_Endpoint_SPC =
			{
				.Header = {.Size = sizeof(USB_MIDI_Descriptor_Jack_Endpoint_t), .Type = AUDIO_DTYPE_CSEndpoint},
				.Subtype = AUDIO_DSUBTYPE_CSEndpoint_General,

				.TotalEmbeddedJacks = 0x01,
				.AssociatedJackID = {0x01}}};

const USB_Descriptor_String_t PROGMEM LanguageString = USB_STRING_DESCRIPTOR_ARRAY(LANGUAGE_ID_ENG);

const USB_Descriptor_String_t PROGMEM ManufacturerString = USB_STRING_DESCRIPTOR(L"OHMMUSICCOLLECTIVE");

const USB_Descriptor_String_t PROGMEM ProductString = USB_STRING_DESCRIPTOR(L"OHMMIDIMOD");

uint16_t CALLBACK_USB_GetDescriptor(const uint16_t wValue,
									const uint16_t wIndex,
									const void **const DescriptorAddress)
{
	const uint8_t DescriptorType = (wValue >> 8);
	const uint8_t DescriptorNumber = (wValue & 0xFF);

	const void *Address = NULL;
	uint16_t Size = NO_DESCRIPTOR;

	switch (DescriptorType)
	{
	case DTYPE_Device:
		Address = &DeviceDescriptor;
		Size = sizeof(USB_Descriptor_Device_t);
		break;
	case DTYPE_Configuration:
		Address = &ConfigurationDescriptor;
		Size = sizeof(USB_Descriptor_Configuration_t);
		break;
	case DTYPE_String:
		switch (DescriptorNumber)
		{
		case STRING_ID_Language:
			Address = &LanguageString;
			Size = pgm_read_byte(&LanguageString.Header.Size);
			break;
		case STRING_ID_Manufacturer:
			Address = &ManufacturerString;
			Size = pgm_read_byte(&ManufacturerString.Header.Size);
			break;
		case STRING_ID_Product:
			Address = &ProductString;
			Size = pgm_read_byte(&ProductString.Header.Size);
			break;
		}

		break;
	}

	*DescriptorAddress = Address;
	return Size;
}
