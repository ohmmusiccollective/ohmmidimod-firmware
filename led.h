/**
 * Driver for the status LED.
 * 
 * Resources:
 *  - Timer/Counter0
 * 
 * Pins:
 *  - PB7 / OC0A
 */
#ifdef __cplusplus
extern "C"
{
#endif
#ifndef _LED_H_
#define _LED_H_

#include <math.h>

#include <avr/io.h>

#include "timer.h"

    typedef enum
    {
        LED_STATE_OFF = 0,
        LED_STATE_ON,
        LED_STATE_BLINK,
        LED_STATE_DIM,
        LED_STATE_PULSE_HALF_CIRCLE,
        LED_STATE_PULSE_SINE,
        LED_STATE_PULSE_TRIANGLE,
    } led_state_e;

    void led_init(void);

    void led_set_state(led_state_e state);

    void led_set_brightness(uint8_t brightness);

#endif
#ifdef __cplusplus
}
#endif